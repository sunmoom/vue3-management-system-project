# 前言

> vue3.0 管理系统

## 学习目标

前端技术栈：vue3.x+vite3+typescript+element-plus+pinia 

后端技术栈：koa2+sequelize+mysql

- 通用组件：
  - 面包屑
  - 标签导航
  - 侧边栏(权限菜单)
  - 自定义 icon（Svg Sprite 图标）
  - 路由检索
- 通用功能：
  - 主题切换 (基于 element-plus)
  - Screenfull 全屏
  - 图片上传
  - 登陆注册（jwt）
  - 权限控制(系统管理：用户管理、角色管理、菜单管理)
  - 权限验证（页面权限、指令权限）

### vue3管理系统源码

前端代码   https://gitee.com/jw-speed/vue3-admin-front.git

后端代码   https://gitee.com/jw-speed/vue3-admin-end.git